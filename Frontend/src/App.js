import {useEffect, useState} from 'react';
import axios from 'axios';
import {Card, Icon, Tab} from 'semantic-ui-react';
import './App.css';

function App() {
  const [results, setResults] = useState([]);
  const [groups, setGroups] = useState({});

  useEffect(() => {
    axios.get("http://localhost:8080/api/animals").then((response) => {
      const groups = response.data.exhibits.reduce((groups, exhibit) => {
        if (!groups[exhibit.type]) {
          groups[exhibit.type] = [];
        }
        groups[exhibit.type].push(exhibit);
        return groups;
      },{});
      setResults(response.data.exhibits);
      setGroups(groups);
    });
  }, []);

  const panes = [{
    menuItem: 'Visual Layout',
    render: () => <Tab.Pane attached={false}><VisualLayout groups={groups} /></Tab.Pane>,
  },{
    menuItem: 'Simple Layout',
    render: () => <Tab.Pane attached={false}><SimpleLayout results={results} /></Tab.Pane>,
  }];

  return (
    <div className="App">
      <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
    </div>
  );
}

const SimpleLayout = ({results = []}) => {
  return (
    <div>
      {results.map((exhibit, i) => {
        const inventory = exhibit.animals.reduce((inventory, animal) => {
          inventory.push(`${animal.type} (size: ${animal.size})`);
          return inventory;
        }, []);
        const inventoryFmt = inventory.join(', ');
        return (
          <div key={i} style={{padding:'5px 0px 5px 0px'}}>
            <div style={{padding:'5px'}}>Exhibit {i+1} (consumed capacity: {exhibit.totalSize})</div>
            <div style={{marginLeft:'10px',padding:'5px',border:'1px solid #CCC',borderRadius:'5px',background:'#EEE'}}>{inventoryFmt}</div>
          </div>
        );
      })}
    </div>
  );
}

const VisualLayout = ({groups}) => (
  <div>
    {Object.entries(groups).map(([name, exhibits]) =>
      <ExhibitGroup name={name} exhibits={exhibits} key={name}/>
    )}
  </div>
)

const ExhibitGroup = ({name, exhibits}) => (
  <Card.Group>
    <Card fluid>
      <Card.Content header={`${name} Exhibits`} style={{textTransform: 'capitalize'}} />
      <Card.Content>
        <Card.Group>
          {exhibits.map((exhibit, i) =>
            <ExhibitEntry name={name} exhibit={exhibit} key={i} />
          )}
        </Card.Group>
      </Card.Content>
      <Card.Content extra>
        <Icon name='user' />
          {exhibits.length === 1 &&
            <span>{exhibits.length} Exhibit</span>
          }
          {exhibits.length !== 1 &&
            <span>{exhibits.length} Exhibits</span>
          }
      </Card.Content>
    </Card>
  </Card.Group>
)

const ExhibitEntry = ({name, exhibit}) => {
  const color = exhibit.totalSize === 20 ? 'blue' : 'yellow';
  return (
    <Card style={{borderTop:`3px solid ${color}`}}>
      <Card.Content header={<ExhibitEntryHeader name={name} exhibit={exhibit} />} />
      <Card.Content style={{background:'#EEE'}}>
        <div style={{paddingTop:'5px'}}>
          <Card.Group>
            {exhibit.animals.map((animal, i) =>
                <div className="exhibit-entry-animal" key={i}>{animal.size}</div>
            )}
          </Card.Group>
        </div>
      </Card.Content>
    </Card>
  );
};

const ExhibitEntryHeader = ({name, exhibit}) => (
  <div className="exhibit-entry-hdr">
    <div>{name}</div>
    <div>consumed capacity: {exhibit.totalSize}</div>
  </div>
)

export default App;
