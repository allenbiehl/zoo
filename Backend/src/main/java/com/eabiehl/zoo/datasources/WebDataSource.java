package com.eabiehl.zoo.datasources;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.eabiehl.zoo.data.Animal;
import com.eabiehl.zoo.data.InputData;
import com.eabiehl.zoo.data.JsonSource;
import com.eabiehl.zoo.data.JsonSourceEntry;
import com.eabiehl.zoo.parsers.AbstractInputParser;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Data source implementation used to retrieve data from a remote json repository.
 * 
 * @author allen
 *
 */
public class WebDataSource implements IDataSource {

	/**
     * Logger instance.
	 */
    private Logger logger;
	
	@Value("${com.eabiehl.zoo.dataSource.url}")
    private String dataSourceUrl;
	
	@Value("${com.eabiehl.zoo.dataSource.apiKey}")
    private String dataSourceApiKey;
	
    /**
     * Constructor for initializing a {@see AbstractStreamParser} instance.
     */
	public WebDataSource() {
	    this.logger = LoggerFactory.getLogger(AbstractInputParser.class);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputData getData() {
        ObjectMapper mapper = new ObjectMapper();
		InputData input = new InputData();
		JsonSource source = null;
		HttpURLConnection httpConnection = null;
		
        try {
        	URL url = new URL(dataSourceUrl);
        	httpConnection = (HttpURLConnection) url.openConnection();
        	httpConnection.setRequestMethod("GET");
        	httpConnection.setRequestProperty("accept", "application/json");
        	httpConnection.setRequestProperty("content-type", "application/json");
        	httpConnection.addRequestProperty("X-Api-Key", dataSourceApiKey);
        	InputStream responseStream = httpConnection.getInputStream();

        	source = mapper.readValue(responseStream, JsonSource.class);
		} 
        catch (IOException e) {
        	this.logger.error("Failed to retrieve remote data source");
		}
        finally {
        	if (httpConnection != null) {
        		httpConnection.disconnect();
        	}
        }
        
        if (source != null) {
        	for (JsonSourceEntry entry : source.getAnimals()) {
        		
        		// Ensure case is inconsistent
        		String species = entry.getSpecies().toLowerCase();
        		
        		input.add(new Animal(species, entry.getSpaces()));
        	}
        }
        return input;
	}

}
