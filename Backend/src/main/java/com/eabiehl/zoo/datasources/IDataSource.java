package com.eabiehl.zoo.datasources;

import com.eabiehl.zoo.data.InputData;

/**
 * Common interface used by all DataSources.
 * 
 * @author allen
 *
 */
public interface IDataSource {

	/**
	 * Return the data associated with the underlying data source.
	 * 
	 * @return Input data parsed from the underlying data source.
	 */
	InputData getData();
}
