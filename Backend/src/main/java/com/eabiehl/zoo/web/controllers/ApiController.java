package com.eabiehl.zoo.web.controllers;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.eabiehl.zoo.data.Animal;
import com.eabiehl.zoo.data.Exhibit;
import com.eabiehl.zoo.data.InputData;
import com.eabiehl.zoo.data.OutputData;
import com.eabiehl.zoo.datasources.IDataSource;
import com.eabiehl.zoo.matchers.IExhibitMatcher;
import com.eabiehl.zoo.matchers.KnapsackExhibitMatcher;
import com.eabiehl.zoo.parsers.IInputParser;

/**
 * Rest controller providing api endpoints.
 * 
 * @author allen
 *
 */
@RestController 
@RequestMapping("api")
public class ApiController {

	@Autowired
	private IInputParser inputParser;
	
	@Autowired
	private IDataSource dataSource;
	
	/**
	 * Default endpoint for returning the most current list of animals from a data source.
	 * 
	 * @return List of exhibits.
	 */
	@GetMapping(path = "animals", produces = "application/json")
    public OutputData getSource() {
    	return this.getCurrentSource();
    }
	
	/**
	 * Default endpoint for returning the most current list of animals from a data source.
	 * 
	 * @return List of exhibits.
	 */
	@GetMapping(path = "animals/current", produces = "application/json")
    public OutputData getCurrentSource() {
    	InputData input = dataSource.getData();
		IExhibitMatcher matcher = new KnapsackExhibitMatcher();
		OutputData output = new OutputData();
    	
		if (input != null) {
			for(Entry<String, List<Animal>> group : input.entrySet()) {
				List<Exhibit> results = matcher.getExhibits(group.getKey(), group.getValue(), 20);
				output.getExhibits().addAll(results);
			}
		}

		return output;
    }
	
	/**
	 * Endpoint for returning static input sources that have been retained for testing purposes. 
	 * 
	 * @param isoDate Iso data that matches the static input source file.
	 * 
	 * @return List of exhibits.
	 */
	@GetMapping(path = "animals/{isoDate}", produces = "application/json")
    public OutputData getStaticSource(@PathVariable("isoDate") String isoDate) {
    	Path path = FileSystems.getDefault().getPath("input", String.format("%s.json", isoDate));
    	InputData input = inputParser.parse(path.toString());
		IExhibitMatcher matcher = new KnapsackExhibitMatcher();
		OutputData output = new OutputData();
    	
		if (input != null) {
			for(Entry<String, List<Animal>> group : input.entrySet()) {
				List<Exhibit> results = matcher.getExhibits(group.getKey(), group.getValue(), 20);
				output.getExhibits().addAll(results);
			}
		}

		return output;
    }
}