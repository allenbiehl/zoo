package com.eabiehl.zoo.web.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
 
/**
 * Home page controller.
 * 
 * @author allen
 *
 */
@Controller
public class DefaultController {
	
	/**
	 * Default home page endpoint used for health checks.
	 * @return
	 */
    @GetMapping("/")
    public ModelAndView home()
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        return modelAndView;
    }
}