package com.eabiehl.zoo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.eabiehl.zoo.datasources.IDataSource;
import com.eabiehl.zoo.datasources.WebDataSource;
import com.eabiehl.zoo.matchers.IExhibitMatcher;
import com.eabiehl.zoo.matchers.KnapsackExhibitMatcher;
import com.eabiehl.zoo.parsers.IInputParser;
import com.eabiehl.zoo.parsers.JsonInputParser;

/**
 * Configuration class for defining runtime settings and wiring dependencies
 * 
 * @author allen
 *
 */
@Configuration
@ComponentScan("com.eabiehl.zoo")
public class AppConfig implements WebMvcConfigurer {

	/**
	 * Register input parser implementation.
	 * 
	 * @return Input parser interface.
	 */
    @Bean
    public IInputParser parser() {
        return new JsonInputParser();
    }
    
    /**
     * Register exhibit matcher implementation.
     * 
     * @return Exhibit matcher implementation.
     */
    @Bean
    public IExhibitMatcher matcher() {
        return new KnapsackExhibitMatcher();
    }
    
    /**
     * Register data source for retrieving input.
     * 
     * @return Data source implementation used to retrieve data.
     */
    @Bean
    public IDataSource dataSource() {
        return new WebDataSource();
    }
    
    /**
     * Configure cors rule to allow react app on 3000 to communication with 8080.
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("http://localhost:3000")
                .allowedMethods("GET");
    }
}