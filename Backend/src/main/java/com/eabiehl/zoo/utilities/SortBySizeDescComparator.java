package com.eabiehl.zoo.utilities;

import java.util.Comparator;

import com.eabiehl.zoo.data.Animal;

/**
 * Comparator used to order animals bottom up by their size. This ensures that
 * as we evaluate an animal, their sizes always order propertly
 * 
 * @author allen
 *
 */
public class SortBySizeDescComparator implements Comparator<Animal> {

	/**
	 * Compare animal a with animal b. 
	 */
	public int compare(Animal a, Animal b) {
		return b.getSize() - a.getSize();
	}
}