package com.eabiehl.zoo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Springboot application entry point.
 * 
 * @author allen
 *
 */
@SpringBootApplication
public class App {

	/**
	 * Configure springboot application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}
}
