package com.eabiehl.zoo.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Class that represents the data returned by the api/animal endpoint containing 
 * a list of exhibits and the animals contained within each exhibit.
 * 
 * @author allen
 */
public class OutputData {
	private List<Exhibit> exhibits;
	
	public OutputData() {
		this.exhibits = new ArrayList<Exhibit>();
	}

	/**
	 * Returns the list of exhibits.
	 * 
	 * @return List of exhibits.
	 */
	public List<Exhibit> getExhibits() {
		return exhibits;
	}
}
