package com.eabiehl.zoo.data;

/**
 * Class that represents a single json source entry that was parsed from a remote json source.
 * 
 * @author allen
 *
 */
public class JsonSourceEntry {
	private String species;
	private Integer spaces;
	
	/**
	 * Returns the animal type / species.
	 * 
	 * @return Animal type / species.
	 */
	public String getSpecies() {
		return species;
	}
	
	/**
	 * Set animal type / species.
	 * 
	 * @param species Animal type / species
	 */
	public void setSpecies(String species) {
		this.species = species;
	}
	
	/**
	 * Returns the size of the animal.
	 * 
	 * @return Animal size.
	 */
	public Integer getSpaces() {
		return spaces;
	}
	
	/**
	 * Set the animal size.
	 * 
	 * @param spaces Animal size.
	 */
	public void setSpaces(Integer spaces) {
		this.spaces = spaces;
	}
}
