package com.eabiehl.zoo.data;

import java.util.List;

/**
 * Class for storing exhibit information including the type of animal the exhibit 
 * contains and a list of animals contained within the exhibit instance.
 * 
 * @author allen
 *
 */
public class Exhibit {
	private String type;
	private List<Animal> animals;
	
	public Exhibit(String type, List<Animal> animals) {
		this.type = type;
		this.animals = animals;
	}
	
	/**
	 * Returns the total sum of all animal sizes.
	 * 
	 * @return Sum of animal sizes.
	 */
	public int getTotalSize() {
		return this.animals.stream().mapToInt(o -> o.getSize()).sum();
	}
	
	/**
	 * Returns the animal type (species).
	 * 
	 * @return Animal type / species.
	 */
	public String getType() {
		return this.type;
	}
	
	/**
	 * Returns list of animal contained within exhibit.
	 * 
	 * @return List of animals.
	 */
	public List<Animal> getAnimals() {
		return this.animals;
	} 
}