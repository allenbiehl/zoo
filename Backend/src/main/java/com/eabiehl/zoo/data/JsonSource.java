package com.eabiehl.zoo.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Class represents the incoming structure returned from the remote json source.
 * 
 * @author allen
 *
 */
public class JsonSource {
	private List<JsonSourceEntry> animals;
	
	public JsonSource() {
		this.animals = new ArrayList<JsonSourceEntry>();
	}

	/**
	 * Returns the list of json source entries that were parsed from the json source.
	 * 
	 * @return List of json source entries.
	 */
	public List<JsonSourceEntry> getAnimals() {
		return animals;
	}

	/**
	 * Sets the list of json source entries that were parsed from the json source.
	 * 
	 * @param animals List of json source entries.
	 */
	public void setAnimals(List<JsonSourceEntry> animals) {
		this.animals = animals;
	}
}
