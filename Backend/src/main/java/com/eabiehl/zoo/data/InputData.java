package com.eabiehl.zoo.data;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * Class that stores the parsed, validated input data source. This differs
 * from source in that it represents a generic structure that crosses data
 * source types. When we retrieve data from a specific data source, we convert
 * the original format into this common format.
 * 
 * This class is backed by a TreeMap which allows us to group all animals by
 * species type. The key represents the species / type and the value represents 
 * a list of one or more animals of that same type. We used a TreeMap as it
 * ensures that the keys are ordered alphabetically and its easy to group 
 * animal types.
 * 
 * @author allen
 *
 */
public class InputData extends TreeMap<String, List<Animal>> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Add animal to its respective species group.
	 * 
	 * @param animal Animal to add to a species group.
	 */
	public void add(Animal animal) {
		if (!this.containsKey(animal.getType())) {
			this.put(animal.getType(), new ArrayList<Animal>());
		}
		this.get(animal.getType()).add(animal);
	}
}
