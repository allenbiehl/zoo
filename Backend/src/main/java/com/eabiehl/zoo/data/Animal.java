package com.eabiehl.zoo.data;

/**
 * Class for storing animal attributes including type and size.
 * 
 * @author allen
 *
 */
public class Animal {
	private String type;
	private int size;
	
	public Animal(String type, int size) {
		this.type = type;
		this.size = size;
	}
	
	/**
	 * Returns the animal type / species.
	 * 
	 * @return Animal type / species.
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Returns the animal size.
	 * 
	 * @return Animal size.
	 */
	public int getSize() {
		return size;
	}
}