package com.eabiehl.zoo.matchers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import com.eabiehl.zoo.data.Animal;
import com.eabiehl.zoo.data.Exhibit;
import com.eabiehl.zoo.utilities.SortBySizeDescComparator;

/**
 * Exhibit matcher implementation that uses the Knapsack algorithm to determine 
 * the most efficient method to group the animals within one or more exhibits.
 * 
 * @author allen
 *
 */
public class KnapsackExhibitMatcher implements IExhibitMatcher {
	
	/**
	 * {@inheritDoc}
	 */
	public List<Exhibit> getExhibits(String type, List<Animal> source, int capacity) {
		
		// Sort animals by size descending
		source.sort(new SortBySizeDescComparator());
		
		List<Exhibit> exhibits = new ArrayList<Exhibit>();

		while (source.size() > 0) {
			List<Integer> indices = this.getAnimalGroupMatches(source, capacity);
	
			// If no matches returned, there is an issue with the source. 
			// i.e. animal.size > capacity
			// i.e. animal.size <= 0
			if (indices.size() == 0) {
				break;
			}
			
			// Get matches
			List<Animal> results = IntStream
				.range(0, source.size())
				.filter(i -> indices.contains(i))
				.mapToObj(i -> source.get(i))
				.collect(Collectors.toList());
	      
			// Remove matching items from source list by index
			indices.stream().mapToInt(i -> i).forEach(source::remove);
	      
			exhibits.add(new Exhibit(type, results));
		}
	  
		return exhibits;
	} 
  
	/**
	 * Used to identify the most efficient combination of animals that can be 
	 * grouped within a single exhibit. We use this method to identify the index
	 * of all animals and then we pull all animals from the source iteratively
	 * to build up x number of exhibits.
	 * 
	 * @param source
	 * @param capacity
	 * @return
	 */
	private List<Integer> getAnimalGroupMatches(List<Animal> source, int capacity) {
		int totalItems = source.size();
	  
		// Initialize matrix with leading row and column
		int[][] matrix = new int[totalItems+1][capacity+1];

		for (int i = 0; i <= totalItems; i++) {
			
			for (int s = 0; s <= capacity; s++) {
        	  
				// Ensure first row and first column cells are 0
				if (i == 0 || s == 0) {
					matrix[i][s] = 0;
				}
				else if (source.get(i-1).getSize() > s) {
					matrix[i][s] = matrix[i-1][s];
				} 
				else {
					matrix[i][s] = Math.max(matrix[i-1][s], matrix[i-1][s - source.get(i-1).getSize()] + source.get(i-1).getSize());
				}
			}
		}

		// Determine the matching indices
		int s = matrix[totalItems][capacity];
		List<Integer> indices = new ArrayList<Integer>();
      
		for (int i = totalItems; i > 0; i--) {
			if (s != matrix[i-1][s]) {
				indices.add(i-1);
				s -= source.get(i-1).getSize();
			}
		}
        
		return indices;
	}
}
