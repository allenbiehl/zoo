package com.eabiehl.zoo.matchers;

import java.util.List;

import com.eabiehl.zoo.data.Animal;
import com.eabiehl.zoo.data.Exhibit;

/**
 * Common interface used by all exhibit matchers.
 * 
 * @author allen
 *
 */
public interface IExhibitMatcher {

	/**
	 * Determines the most efficient scheme for grouping animals by type and within the exhibit capacity.
	 * 
	 * @param type Animal type
	 * @param source List of animals to generate the exhibit groups.
	 * @param capacity Max size of animals that can be stored in an exhibit.
	 * @return List of exhibits.
	 */
	List<Exhibit> getExhibits(String type, List<Animal> source, int capacity);
}
