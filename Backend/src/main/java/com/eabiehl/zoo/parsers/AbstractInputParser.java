package com.eabiehl.zoo.parsers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.eabiehl.zoo.data.InputData;

/**
 * Abstract class that provides the foundation for all Stream Parser implementations.
 * If you create additional Stream Parser implementations, you should extend this 
 * class as it consolidates common functionality shared across all instances, such as 
 * retrieving the appropriate resource and ensuring file streams are closed after 
 * parsing is completed. 
 * 
 * @author eabiehl
 */
public abstract class AbstractInputParser implements IInputParser {
	
	/**
     * Logger instance.
	 */
    private Logger logger;
    
    /**
     * Constructor for initializing a {@see AbstractStreamParser} instance.
     */
	public AbstractInputParser() {
	    this.logger = LoggerFactory.getLogger(AbstractInputParser.class);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public InputData parse(String resourcePath) {
    	this.logger.debug(String.format("Parsing resource %s", resourcePath));
    	
    	// Check if file exists on file system
    	File file = new File(resourcePath);

    	if (file.exists()) {
    		try {
    			return this.read(new FileInputStream(resourcePath));
    		} 
    		catch(FileNotFoundException e) {
            	this.logger.error("File does not exist");
    		}
    		return null;
    	}
		
    	// Check if file exists on classpath
		ClassLoader classLoader = getClass().getClassLoader();

		try {
        	InputStream inputstream = classLoader.getResourceAsStream(resourcePath);
        	return this.read(inputstream);
		} 
		catch(Exception e) {
        	this.logger.error("Resource does not exist");
		}
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public InputData parse(InputStream inputStream) {
		this.logger.debug("Parsing input stream");
		try {
			return this.read(inputStream);
		} 
		catch(Exception e) {
        	this.logger.error("Failed to parse input stream", e);
		}
		finally
		{
			this.closeStream(inputStream);
		}
		return null;
	}
	
	/**
	 * Implementation specific method used to read the specific file format.
	 * 
	 * @param inputStream {@see InputStream} to parse.
	 * @return Parsed {@see InputData} instance containing the two dimensional 
	 * grid and list of words to match.
	 */
	protected abstract InputData read(InputStream inputStream);
	
	/**
	 * Ensure that the input stream is closed to prevent memory leak.
	 * 
	 * @param inputStream {@see InputStream} to close.
	 */
	protected void closeStream(InputStream inputStream) {
		if (inputStream != null) {
			this.logger.debug("Closing input stream");
			try {
				inputStream.close();
				this.logger.debug("Input stream closed");
			} 
			catch (IOException e) {
				this.logger.error("Failed to close input stream", e);
			}
		}
	}
}
