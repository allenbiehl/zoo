package com.eabiehl.zoo.parsers;

import java.io.IOException;
import java.io.InputStream;

import com.eabiehl.zoo.data.Animal;
import com.eabiehl.zoo.data.InputData;
import com.eabiehl.zoo.data.JsonSource;
import com.eabiehl.zoo.data.JsonSourceEntry;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Class that is used to parse a json input source. Typically we use this implementation
 * to test static resources stored with main/resources.
 * 
 * @author allen
 *
 */
public class JsonInputParser extends AbstractInputParser {

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected InputData read(InputStream inputStream) {
        ObjectMapper mapper = new ObjectMapper();
		InputData input = new InputData();
		JsonSource source = null;
		
        try {
        	source = mapper.readValue(inputStream, new TypeReference<JsonSource>(){});
		} 
        catch (IOException e) {
			throw new RuntimeException("Failed to parse input stream", e);
		}
        
        if (source != null) {
        	for (JsonSourceEntry entry : source.getAnimals()) {
        		
        		// Ensure case is inconsistent
        		String species = entry.getSpecies().toLowerCase();
        		
        		input.add(new Animal(species, entry.getSpaces()));
        	}
        }

		return input;
	}
}
