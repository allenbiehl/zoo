package com.eabiehl.zoo.parsers;

import java.io.InputStream;

import com.eabiehl.zoo.data.InputData;

/**
 * Common interface used by all Input parsers.
 * 
 * @author allen
 *
 */
public interface IInputParser {

	/**
	 * Parses a given resource path. This can either be an external resource or a 
	 * resource on the class path.
	 * 
	 * @param resourcePath Resource path to use for parsing.
	 *  
	 * @return Input data extracted from the resource specified.
	 */
	InputData parse(String resourcePath);
	
	/**
	 * Parses a given input stream. 
	 * 
	 * @param is Input stream to parse.
	 */
	InputData parse(InputStream is);
}
