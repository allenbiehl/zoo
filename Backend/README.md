### BTI360 Code Challenge

#### General

##### Package Manager

- Maven

##### Runtime

- JavaSE-8

##### Install Tools

- Install Maven
- Install Node

#### Build & Test

##### Configure Application

- Open application.properties
- Enter api key

##### Package Application

```
cd <project_root>/Backend
mvn package
```

##### Start Backend Application

```
cd <project_root>/Backend/target
java -jar zoo-0.0.1-SNAPSHOT.jar
```

##### Start Frontend Application

```
cd <project_root>/Frontend
npm install
npm start
```

##### View Application

React App

- Open Browser
- Enter "localhost:3000/api"

Api Endpoints

Execute Get request using Postman, Browser, or Curl, or an equivalent tool

 - http://localhost:8080/api/animals
 - http://localhost:8080/api/animals/current
 - http://localhost:8080/api/animals/20220522
 - http://localhost:8080/api/animals/20220523

Note: Must add X-Api-Key http header value.

#### Areas for Improvement

- Create Unit Tests
- Add Error handling
- Add Debug logging
- Add packaging
- Major UI refactoring